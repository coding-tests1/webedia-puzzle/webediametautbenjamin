package com.example.webediametautbenjamin.ui.game.recyclerview

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.ViewTreeObserver
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.webediametautbenjamin.R
import com.example.webediametautbenjamin.databinding.FragmentGameRecyclerviewBinding
import com.example.webediametautbenjamin.ui.base.BaseFragment
import com.example.webediametautbenjamin.utils.injector
import kotlinx.android.synthetic.main.fragment_game_recyclerview.*

class PuzzleRecyclerViewFragment : BaseFragment<PuzzleRecyclerViewViewModel, FragmentGameRecyclerviewBinding>(
    R.layout.fragment_game_recyclerview,
    PuzzleRecyclerViewViewModel::class.java
) {

    private val puzzleRecyclerViewAdapter = PuzzleRecyclerViewAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.context?.let { viewModel.setContext(it) }
    }

    override fun inject() {
        injector.inject(this)

    }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.adapter = puzzleRecyclerViewAdapter
    }

    override fun initUi() {

        puzzles_recycler_view.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = puzzleRecyclerViewAdapter
            it.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    startPostponedEnterTransition()
                    it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }

        button_option.setOnClickListener {
            findNavController().navigate(R.id.action_puzzleRecyclerViewFragment_to_settingsGameFragment)
        }


    }

    override fun initObservers() {

        viewModel.puzzles.observe(viewLifecycleOwner, Observer { puzzles ->

            puzzles?.let {
                puzzleRecyclerViewAdapter.submitList(it)
            }
        })

        viewModel.step.observe(viewLifecycleOwner, Observer {

        })

    }

    override fun initTransition() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }


}
