package com.example.webediametautbenjamin.ui.game

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class GameViewModel @Inject constructor() : ViewModel()