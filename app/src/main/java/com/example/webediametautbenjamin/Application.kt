package com.example.webediametautbenjamin

import android.app.Application
import com.example.webediametautbenjamin.di.components.ApplicationComponent
import com.example.webediametautbenjamin.di.components.DaggerApplicationComponent
import com.example.webediametautbenjamin.di.components.DaggerComponentProvider
import com.example.webediametautbenjamin.di.modules.DatabaseModule
import com.example.webediametautbenjamin.di.modules.ViewModelsModule

class Application : Application(), DaggerComponentProvider {

    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .databaseModule(DatabaseModule)
            .viewModelsModule(ViewModelsModule)
            .build()
    }


}