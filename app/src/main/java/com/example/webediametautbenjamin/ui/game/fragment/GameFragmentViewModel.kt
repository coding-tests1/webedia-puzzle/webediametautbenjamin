package com.example.webediametautbenjamin.ui.game.fragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.webediametautbenjamin.data.Data
import com.example.webediametautbenjamin.data.local.database.entities.ProfileEntity
import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity
import com.example.webediametautbenjamin.data.repositories.ProfileRepository
import com.example.webediametautbenjamin.data.repositories.PuzzlesRepository
import javax.inject.Inject

class GameFragmentViewModel @Inject constructor(
    private val puzzlesRepository: PuzzlesRepository,
    profileRepository: ProfileRepository
) : ViewModel() {
    private val _puzzleId: MutableLiveData<Int> = MutableLiveData()
    private val _data: LiveData<Data<PuzzleEntity>> =
        Transformations.switchMap(_puzzleId) { puzzlesRepository.getPuzzle(it) }
    private val _dataProfile: LiveData<Data<ProfileEntity>> = profileRepository.getProfile()

    val image: MutableLiveData<Bitmap> = MutableLiveData()

    var size: MutableLiveData<Int> = MutableLiveData()

    val profile: LiveData<ProfileEntity> = Transformations.map(_dataProfile) { it.data }

    var arrayBitmap: MutableLiveData<ArrayList<Bitmap>> = MutableLiveData()
    val arrayBitmapSaved: MutableLiveData<ArrayList<Bitmap>> = MutableLiveData()

    val answer: LiveData<String> = Transformations.map(_data) { it.data?.answer }
    val imageName: LiveData<String> = Transformations.map(_data) { it.data?.imageName }

    var userWin: MutableLiveData<Boolean> = MutableLiveData()

    var userFail: MutableLiveData<Boolean> = MutableLiveData()


    fun setWin(bool: Boolean) {
        userWin.value = bool

    }

    fun setFail(bool: Boolean) {
        userFail.value = bool
    }

    fun setPuzzleId(id: Int) {
        _puzzleId.value = id
    }

    fun setImage(context: Context) {
        val id = context.resources.getIdentifier(imageName.value, "drawable", context.packageName)
        image.value = BitmapFactory.decodeResource(context.resources, id)
    }

    fun setSize(parts: Int) {
        size.value = parts
    }

    fun checkUserAnswer(userAnswer: String): Boolean {
        return userAnswer.toLowerCase() == answer.value?.toLowerCase()
    }


}