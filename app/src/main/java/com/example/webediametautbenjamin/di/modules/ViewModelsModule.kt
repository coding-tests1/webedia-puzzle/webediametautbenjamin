package com.example.webediametautbenjamin.di.modules

import androidx.lifecycle.ViewModel
import com.example.webediametautbenjamin.data.repositories.ProfileRepository
import com.example.webediametautbenjamin.data.repositories.PuzzlesRepository
import com.example.webediametautbenjamin.di.ViewModelFactory
import com.example.webediametautbenjamin.ui.game.GameViewModel
import com.example.webediametautbenjamin.ui.game.fragment.GameFragmentViewModel
import com.example.webediametautbenjamin.ui.game.recyclerview.PuzzleRecyclerViewViewModel
import com.example.webediametautbenjamin.ui.game.settings.SettingsGameViewModel
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.multibindings.IntoMap
import javax.inject.Provider
import kotlin.reflect.KClass

@Module
object ViewModelsModule {

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Reusable
    @JvmStatic
    @Provides
    fun providesViewModelFactory(providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): ViewModelFactory =
        ViewModelFactory(providerMap)

    //Insert views here
    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(GameViewModel::class)
    fun providesGameViewModel(): ViewModel = GameViewModel()


    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(PuzzleRecyclerViewViewModel::class)
    fun providesPuzzleRecyclerViewViewModel(
        puzzlesRepository: PuzzlesRepository,
        profileRepository: ProfileRepository
    ): ViewModel = PuzzleRecyclerViewViewModel(puzzlesRepository, profileRepository)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(GameFragmentViewModel::class)
    fun providesGameFragmentViewModel(
        puzzlesRepository: PuzzlesRepository,
        profileRepository: ProfileRepository
    ): ViewModel = GameFragmentViewModel(puzzlesRepository, profileRepository)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(SettingsGameViewModel::class)
    fun providesSettingsFragmentViewModel(profileRepository: ProfileRepository): ViewModel =
        SettingsGameViewModel(profileRepository)


}
