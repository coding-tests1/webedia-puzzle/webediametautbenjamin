package com.example.webediametautbenjamin.ui.game.fragment

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import java.util.ArrayList

class GameFragmentGridAdapter(val context: Context, var arrayBitmap: ArrayList<Bitmap>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val imageView: ImageView
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = ImageView(context)
            imageView.adjustViewBounds = true
            imageView.scaleType = ImageView.ScaleType.FIT_XY
            imageView.setPadding(0, 0, 0, 0)
        } else {
            imageView = convertView as ImageView
        }

        imageView.setImageBitmap(arrayBitmap[position])
        return imageView
    }

    override fun getItem(position: Int): Any {
        return arrayBitmap[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return arrayBitmap.size
    }


}
