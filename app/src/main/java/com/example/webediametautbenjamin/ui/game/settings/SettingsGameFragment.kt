package com.example.webediametautbenjamin.ui.game.settings

import android.os.Bundle
import android.transition.TransitionInflater
import androidx.lifecycle.Observer
import com.example.webediametautbenjamin.R
import com.example.webediametautbenjamin.ui.base.BaseFragment
import com.example.webediametautbenjamin.utils.injector
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsGameFragment :
    BaseFragment<SettingsGameViewModel, com.example.webediametautbenjamin.databinding.FragmentSettingsBinding>(
        R.layout.fragment_settings,
        SettingsGameViewModel::class.java
    ) {


    override fun inject() {
        injector.inject(this)

    }

    override fun initBinding() {
        binding.viewModel = viewModel
    }

    override fun initUi() {

        complexity_picker.minValue = 3
        complexity_picker.maxValue = 10
        complexity_picker.setOnValueChangedListener { picker, oldVal, newVal ->
            viewModel.changeComplexity(newVal)
        }


    }

    override fun initObservers() {

        viewModel.complexity.observe(viewLifecycleOwner, Observer {


        })
    }


    override fun initTransition() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
        startPostponedEnterTransition()
    }
}