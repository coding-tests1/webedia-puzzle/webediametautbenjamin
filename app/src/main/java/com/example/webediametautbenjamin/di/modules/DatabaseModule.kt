package com.example.webediametautbenjamin.di.modules

import android.content.Context
import androidx.room.Room
import com.example.webediametautbenjamin.BuildConfig
import com.example.webediametautbenjamin.data.local.database.AppDatabase
import com.example.webediametautbenjamin.data.local.database.dao.ProfileDao
import com.example.webediametautbenjamin.data.local.database.dao.PuzzleDao
import com.example.webediametautbenjamin.data.local.file.JsonParser
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideAppDatabase(context: Context):
            AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, BuildConfig.DATABASE_NAME)
        .allowMainThreadQueries()
        .build()

    @Reusable
    @JvmStatic
    @Provides
    fun providePuzzleDao(appDatabase: AppDatabase): PuzzleDao = appDatabase.puzzleDao()


    @Reusable
    @JvmStatic
    @Provides
    fun provideJsonParser(appDatabase: AppDatabase): JsonParser = appDatabase.jsonParser()

    @Reusable
    @JvmStatic
    @Provides
    fun provideProfileDao(appDatabase: AppDatabase): ProfileDao = appDatabase.profileDao()
}