package com.example.webediametautbenjamin.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.webediametautbenjamin.data.local.database.dao.ProfileDao
import com.example.webediametautbenjamin.data.local.database.dao.PuzzleDao
import com.example.webediametautbenjamin.data.local.database.entities.ProfileEntity
import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity
import com.example.webediametautbenjamin.data.local.file.JsonParser

@Database(
    version = 1,
    entities = [
        PuzzleEntity::class,
        ProfileEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun puzzleDao(): PuzzleDao
    abstract fun profileDao(): ProfileDao
    abstract fun jsonParser(): JsonParser
}