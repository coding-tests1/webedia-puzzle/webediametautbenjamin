package com.example.webediametautbenjamin.di.components

interface DaggerComponentProvider {
    val component: ApplicationComponent
}