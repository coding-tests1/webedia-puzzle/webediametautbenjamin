package com.example.webediametautbenjamin.data.local.database.dao

import androidx.annotation.WorkerThread
import androidx.room.*

@Dao
interface BaseDao<in T> {
    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T?)

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMultiple(vararg entity: T)

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entities: List<T>?)


    @WorkerThread
    @Update
    fun update(entity: T)

    @WorkerThread
    @Update
    fun updateMultiple(vararg entity: T)

    @WorkerThread
    @Update
    fun updateAll(entities: List<T>)

    @WorkerThread
    @Delete
    fun delete(entity: T)

    @WorkerThread
    @Delete
    fun deleteMultiple(vararg entity: T)

    @WorkerThread
    @Delete
    fun deleteAll(entities: List<T>)

}