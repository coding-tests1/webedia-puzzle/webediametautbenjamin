package com.example.webediametautbenjamin.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "puzzles")
data class PuzzleEntity(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "image_name")
    val imageName: String,

    @ColumnInfo(name = "answer")
    val answer: String

) {
    fun getImageAsJpg(): String {
        return "$imageName.jpg"
    }
}