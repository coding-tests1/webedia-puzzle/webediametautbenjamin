package com.example.webediametautbenjamin.data.local.file

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.room.Dao
import com.example.webediametautbenjamin.BuildConfig
import com.example.webediametautbenjamin.data.local.file.dto.PuzzleDto
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

@Dao
abstract class JsonParser {

    fun getPuzzleJsonData(context: Context): LiveData<List<PuzzleDto>> {

        //Read Json Part
        val json: String

        val inputStream = context.assets.open(BuildConfig.JSON_PUZZLES_PATH)
        val size = inputStream.available()
        val buffer = ByteArray(size)

        inputStream.use { it.read(buffer) }

        json = String(buffer)

        //Transform Json to Object
        val moshi = Moshi.Builder().build()

        val type = Types.newParameterizedType(List::class.java, PuzzleDto::class.java)

        val adapter = moshi.adapter<List<PuzzleDto>>(type)


        //Transform result into live data
        val mediator: MediatorLiveData<List<PuzzleDto>> = MediatorLiveData()

        mediator.value = adapter.fromJson(json)

        return mediator
    }

}