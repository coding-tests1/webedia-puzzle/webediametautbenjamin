package com.example.webediametautbenjamin.ui.game.recyclerview

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.webediametautbenjamin.data.Data
import com.example.webediametautbenjamin.data.local.database.entities.ProfileEntity
import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity
import com.example.webediametautbenjamin.data.repositories.ProfileRepository
import com.example.webediametautbenjamin.data.repositories.PuzzlesRepository
import javax.inject.Inject

class PuzzleRecyclerViewViewModel @Inject constructor(private val puzzlesRepository: PuzzlesRepository, profileRepository: ProfileRepository) : ViewModel(){
    private var _context: MutableLiveData<Context> = MutableLiveData()
    private val _data: LiveData<Data<List<PuzzleEntity>>> = Transformations.switchMap(_context) { puzzlesRepository.getPuzzles(it)}
    private val _dataProfile: LiveData<Data<ProfileEntity>> = profileRepository.getProfile()

    val puzzles: LiveData<List<PuzzleEntity>?> = Transformations.map(_data) {it.data}

    val step: LiveData<Int> = Transformations.map(_dataProfile) { it.data?.step }
    val loading: LiveData<Boolean> = Transformations.map(_data) {it.status == Data.Status.LOADING}

    fun setContext(context: Context){
        _context.value = context
    }
}