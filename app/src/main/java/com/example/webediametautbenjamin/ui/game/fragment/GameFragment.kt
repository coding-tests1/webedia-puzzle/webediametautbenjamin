package com.example.webediametautbenjamin.ui.game.fragment


import android.os.Bundle
import android.transition.TransitionInflater
import android.view.ViewTreeObserver
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.webediametautbenjamin.R
import com.example.webediametautbenjamin.databinding.FragmentGameBinding
import com.example.webediametautbenjamin.ui.base.BaseFragment
import com.example.webediametautbenjamin.utils.injector
import com.example.webediametautbenjamin.utils.multipleShuffle
import com.example.webediametautbenjamin.utils.splitImageToArray
import com.example.webediametautbenjamin.utils.switchCentralPart
import kotlinx.android.synthetic.main.fragment_game.*

class GameFragment : BaseFragment<GameFragmentViewModel, FragmentGameBinding>
    (R.layout.fragment_game, GameFragmentViewModel::class.java) {

    private val args: GameFragmentArgs by navArgs()

    private lateinit var gridAdapter: GameFragmentGridAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        viewModel.setPuzzleId(args.puzzleId)
        viewModel.setWin(false)
        viewModel.setFail(false)
    }

    override fun initUi() {


        grid_content_view.also {
            it.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    startPostponedEnterTransition()
                    it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }
        button_user_send.setOnClickListener {

            if (viewModel.checkUserAnswer(edittext_user_answer.text.toString())) {
                winAnimation()
                viewModel.setFail(false)
                viewModel.setWin(true)

            } else {
                val animationShake = AnimationUtils.loadAnimation(this.context, R.anim.shake_animation)
                edittext_user_answer.startAnimation(animationShake)
                viewModel.setWin(false)
                viewModel.setFail(true)
            }
        }
    }

    override fun inject() {
        injector.inject(this)

    }

    override fun initObservers() {
        viewModel.imageName.observe(viewLifecycleOwner, Observer {
            it?.let { imageName ->
                this.context?.let { it -> viewModel.setImage(it) }
            }
        })

        viewModel.image.observe(viewLifecycleOwner, Observer {
            it?.let {

                viewModel.size.value?.let {
                    setupGrid()
                }


            }
        })

        viewModel.answer.observe(viewLifecycleOwner, Observer {

        })

        viewModel.userWin.observe(viewLifecycleOwner, Observer {

        })

        viewModel.profile.observe(viewLifecycleOwner, Observer {profile ->
            profile?.let {
                viewModel.size.value = profile.complexity
                viewModel.image.value?.let {
                    setupGrid()
                }
            }

        })

    }

    override fun initBinding() {
        binding.viewModel = viewModel

    }

    override fun initTransition() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()

    }

    private fun setupGrid() {
        viewModel.arrayBitmap.value = viewModel.image.value?.splitImageToArray(viewModel.size.value!!)
        viewModel.arrayBitmapSaved.value = viewModel.image.value?.splitImageToArray(viewModel.size.value!!)
        val centralPart = viewModel.arrayBitmap.value?.get((viewModel.arrayBitmap.value!!.lastIndex / 2))

        viewModel.arrayBitmap.value?.multipleShuffle(2)
        viewModel.arrayBitmap.value?.switchCentralPart(centralPart!!)
        gridAdapter = GameFragmentGridAdapter(this.context!!, viewModel.arrayBitmap.value!!)
        binding.adapter = gridAdapter
    }

    private fun winAnimation() {
        gridAdapter.arrayBitmap = viewModel.arrayBitmapSaved.value!!
        val animationController = AnimationUtils.loadLayoutAnimation(this.context, R.anim.layout_animation_grid)

        gridview_game.layoutAnimation = animationController

        for (i in 0 until gridAdapter.arrayBitmap.size) {
            gridAdapter.arrayBitmap[i] = viewModel.arrayBitmapSaved.value!![i]
            gridAdapter.notifyDataSetChanged()
        }

    }
}