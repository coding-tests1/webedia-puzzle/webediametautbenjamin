package com.example.webediametautbenjamin.data.repositories


import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.webediametautbenjamin.data.Data
import com.example.webediametautbenjamin.data.local.database.dao.ProfileDao
import com.example.webediametautbenjamin.data.local.database.entities.ProfileEntity
import javax.inject.Inject

class ProfileRepository @Inject constructor(
    private val profileDao: ProfileDao
) {


    fun getProfile(): LiveData<Data<ProfileEntity>> {
        val mediator: MediatorLiveData<Data<ProfileEntity>> = MediatorLiveData()

        mediator.value = Data.loading()

        mediator.addSource(getProfileFromRoom()) {
            when (it) {
                //Si aucun profil n'a encore été setup
                null -> {
                    setupProfile(ProfileEntity(1, 3, 0))
                }
                else -> {
                    mediator.value = Data.success(it)
                }
            }

        }

        return mediator
    }

    fun changeProfileComplexity(oldProfile: ProfileEntity, newComplexity: Int) {

        setupProfile(ProfileEntity(oldProfile.id, newComplexity, oldProfile.step))

    }

    fun changeProfileStep(newStep: Int) {
        val mediator: MediatorLiveData<Data<ProfileEntity>> = MediatorLiveData()

        mediator.addSource(getProfileFromRoom()) {
            it.step = newStep
            profileDao.insert(it)
        }
    }


    private fun setupProfile(profile: ProfileEntity) {
        profileDao.insert(profile)
    }


    private fun getProfileFromRoom(): LiveData<ProfileEntity> {
        return profileDao.getProfile()
    }


}