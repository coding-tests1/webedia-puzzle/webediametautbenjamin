package com.example.webediametautbenjamin.data.local.file.dto

import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PuzzleDto(
    @Json(name = "i")
    val id: Int,

    @Json(name = "in")
    val imageName: String,

    @Json(name = "a")
    val answer: String

) {
    fun toEntity(): PuzzleEntity {
        return PuzzleEntity(
            id,
            imageName,
            answer
        )
    }
}