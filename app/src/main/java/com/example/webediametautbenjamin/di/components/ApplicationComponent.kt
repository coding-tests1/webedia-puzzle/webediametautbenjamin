package com.example.webediametautbenjamin.di.components

import android.content.Context
import com.example.webediametautbenjamin.di.modules.DatabaseModule
import com.example.webediametautbenjamin.di.modules.ViewModelsModule
import com.example.webediametautbenjamin.ui.game.fragment.GameFragment
import com.example.webediametautbenjamin.ui.game.recyclerview.PuzzleRecyclerViewFragment
import com.example.webediametautbenjamin.ui.game.settings.SettingsGameFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        DatabaseModule::class,
        ViewModelsModule::class
    ]
)


interface ApplicationComponent {


    fun inject(fragment: PuzzleRecyclerViewFragment)
    fun inject(fragment: GameFragment)
    fun inject(fragment: SettingsGameFragment)


    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder

        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun viewModelsModule(viewModelsModule: ViewModelsModule): Builder

        fun build(): ApplicationComponent
    }
}