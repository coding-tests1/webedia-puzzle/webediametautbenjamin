package com.example.webediametautbenjamin.ui.game.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.webediametautbenjamin.data.Data
import com.example.webediametautbenjamin.data.local.database.entities.ProfileEntity
import com.example.webediametautbenjamin.data.repositories.ProfileRepository
import javax.inject.Inject

class SettingsGameViewModel @Inject constructor(private val profileRepository: ProfileRepository) : ViewModel() {
    private val _dataProfile: LiveData<Data<ProfileEntity>> = profileRepository.getProfile()

    val complexity: LiveData<Int> = Transformations.map(_dataProfile) { it.data?.complexity }


    fun changeComplexity(value: Int) {

        _dataProfile.value?.data?.let { profileRepository.changeProfileComplexity(it, value) }
    }
}