package com.example.webediametautbenjamin.ui.game.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.webediametautbenjamin.R
import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity
import kotlinx.android.extensions.LayoutContainer

class PuzzleRecyclerViewAdapter : ListAdapter<PuzzleEntity, PuzzleRecyclerViewAdapter.ViewHolder>
    (DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = DataBindingUtil.inflate<com.example.webediametautbenjamin.databinding.FragmentGameItemBinding>(
            LayoutInflater.from(parent.context)
            , R.layout.fragment_game_item, parent, false
        )


        return ViewHolder(binding.root, binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        getItem(position).let { puzzle ->

            holder.itemView.tag = puzzle
            holder.itemView.setOnClickListener {
                //holder.itemView.findNavController().navigate(R.id.booksDetailFragment)
                holder.itemView.findNavController().navigate(
                    R.id.action_puzzleRecyclerViewFragment_to_gameFragment,
                    bundleOf("puzzleId" to puzzle.id),
                    null
                )
            }
            holder.bind(puzzle)

        }

    }


    class ViewHolder(
        override val containerView: View,
        private val binding: com.example.webediametautbenjamin.databinding.FragmentGameItemBinding
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {


        fun bind(puzzle: PuzzleEntity) {

            binding.puzzle = puzzle
        }


    }

    class DiffCallback : DiffUtil.ItemCallback<PuzzleEntity>() {
        override fun areItemsTheSame(oldItem: PuzzleEntity, newItem: PuzzleEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PuzzleEntity, newItem: PuzzleEntity): Boolean {
            return oldItem == newItem
        }
    }
}