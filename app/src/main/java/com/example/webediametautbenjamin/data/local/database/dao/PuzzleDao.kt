package com.example.webediametautbenjamin.data.local.database.dao


import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity

@Dao
interface PuzzleDao : BaseDao<PuzzleEntity> {

    @Query("SELECT * FROM puzzles")
    fun getAll(): LiveData<List<PuzzleEntity>>

    @Query("SELECT * FROM puzzles WHERE id = :id")
    fun getOne(id: Int): LiveData<PuzzleEntity>


}