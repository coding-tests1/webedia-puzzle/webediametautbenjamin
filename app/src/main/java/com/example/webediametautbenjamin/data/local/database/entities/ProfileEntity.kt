package com.example.webediametautbenjamin.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "profiles")
data class ProfileEntity(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "complexity")
    var complexity: Int,

    @ColumnInfo(name = "step")
    var step: Int

)
