package com.example.webediametautbenjamin.utils

import android.graphics.Bitmap

fun Bitmap.splitImageToArray(parts: Int): ArrayList<Bitmap> {

    val partsSize = this.width / parts

    val bitmapArray: ArrayList<Bitmap> = ArrayList()

    for (i in 0 until parts) {
        for (j in 0 until parts) {
            bitmapArray.add(Bitmap.createBitmap(this, partsSize * j, partsSize * i, partsSize, partsSize))
        }
    }

    return bitmapArray

}

fun ArrayList<Bitmap>.multipleShuffle(complexity: Int) {

    for (i in 0 until complexity) {
        this.shuffle()
    }
}

fun ArrayList<Bitmap>.switchCentralPart(part: Bitmap) {

    val actualIndexOfCentralPart = this.indexOf(part)

    this[actualIndexOfCentralPart] = this.get(this.lastIndex)

    this[this.lastIndex] = part

}
