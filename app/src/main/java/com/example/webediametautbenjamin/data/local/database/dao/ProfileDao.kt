package com.example.webediametautbenjamin.data.local.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.webediametautbenjamin.data.local.database.entities.ProfileEntity

@Dao
interface ProfileDao : BaseDao<ProfileEntity> {

    @Query("SELECT * FROM profiles")
    fun getProfile(): LiveData<ProfileEntity>


}