package com.example.webediametautbenjamin.data.repositories

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.webediametautbenjamin.data.Data
import com.example.webediametautbenjamin.data.local.database.dao.PuzzleDao
import com.example.webediametautbenjamin.data.local.database.entities.PuzzleEntity
import com.example.webediametautbenjamin.data.local.file.JsonParser
import com.example.webediametautbenjamin.data.local.file.dto.PuzzleDto
import javax.inject.Inject

class PuzzlesRepository @Inject constructor(
    private val jsonParser: JsonParser,
    private val puzzleDao: PuzzleDao
) {

    fun getPuzzles(context: Context): LiveData<Data<List<PuzzleEntity>>> {
        val mediator: MediatorLiveData<Data<List<PuzzleEntity>>> = MediatorLiveData()

        mediator.value = Data.loading()

        mediator.addSource(getPuzzlesFromRoom()) {
            mediator.value = Data.success(it)
        }

        mediator.addSource(getPuzzlesFromAssets(context)) {
            puzzleDao.insertAll(it.map { puzzleDto ->
                puzzleDto.toEntity()
            })
        }

        return mediator

    }

    fun getPuzzle(id: Int): LiveData<Data<PuzzleEntity>> {
        val mediator: MediatorLiveData<Data<PuzzleEntity>> = MediatorLiveData()

        mediator.value = Data.loading()

        mediator.addSource(getPuzzleFromRoom(id)) {
            mediator.value = Data.success(it)
        }


        return mediator
    }


    private fun getPuzzlesFromAssets(context: Context): LiveData<List<PuzzleDto>> {

        return jsonParser.getPuzzleJsonData(context)
    }

    private fun getPuzzlesFromRoom(): LiveData<List<PuzzleEntity>> {
        return puzzleDao.getAll()
    }

    private fun getPuzzleFromRoom(id: Int): LiveData<PuzzleEntity> {
        return puzzleDao.getOne(id)
    }


}